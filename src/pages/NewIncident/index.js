import React from 'react';
import {Link} from 'react-router-dom';
import {FiArrowLeft} from 'react-icons/fi';

import './styles.css';

import logoImg from '../../assets/hero.svg';

export default function Profile() {
    return (
        <div className="new-incident-container">
            <div className="content">
                <section>
                    <img src={logoImg} alt="Be the Hero"/>
                    <h1>Cadastrar novo caso</h1>
                    <p> Descreva o caso detalhadamente para encontrar uma pessoa para resolver o problema.</p>

                    <Link className="back-link" to="/profile">
                        <FiArrowLeft size={16} color="#E02141"/>
                        Voltar para home
                    </Link>

                </section>

                <form>
                    <input placeholder="Título do caso"/>
                    <textarea placeholder="Descrição"/>
                    <input placeholder="Valor em reais"/>

                    
                    <button className="button" type="submit">Cadastrar</button>

                </form>
            </div>
        </div>
    );
}