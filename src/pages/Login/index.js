import React from 'react';
import {Link} from 'react-router-dom';
import {FiLogIn} from 'react-icons/fi';

import './styles.css';

import logoImg from '../../assets/hero.svg';

import herosImg from '../../assets/fazer_login.png';

export default function Login(){
    return (
        <div className="login-container">
            <section className="form">
                <img src={logoImg} alt="Be The Hero"/>

                <form>
                    <h1>Faça seu login</h1>
                    
                    <input placeholder=" Sua ID"/>
                    <button className="button" type="submit">Entrar</button>
                    <Link className="back-link" to="/register">
                        <FiLogIn size={16} color="#E02141"/>
                        Não tenho cadastro
                    </Link>
                </form>

            </section>

            <img className="imagem" src={herosImg} alt="Login"/>
        </div>
    );
}